export class Level{
    public level:number;
    public name:string;
    public turnos:number|null;

    constructor(){
        this.level=0;
        this.name="";
        this.turnos=null;
    }
}