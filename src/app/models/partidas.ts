import { Level } from "./level";

export class Partidas{
    public tablero:any[];
    public win:boolean;
    public level:Level
    public turnosJugados:number;

    constructor(){
        this.tablero = [];
        this.win = false;
        this.level = new Level();
        this.turnosJugados = 0;
    }
}