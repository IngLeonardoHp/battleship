import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(public router:Router) { }

  ngOnInit(): void {
  }

  goTablero(){
    this.router.navigate([""]);
  }
  
  goConfiguracion(){
    this.router.navigate(["configuracion"]);
  }
  
  goJuegos(){
    this.router.navigate(["lista-de-juegos-terminados"]);
  }

}
