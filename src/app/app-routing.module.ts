import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
      path:"",
      loadChildren:()=> import('./tablero/tablero.module').then(m => m.TableroModule)
  },
  {
    path:"configuracion",
    loadChildren:()=>import('./configuracion/configuracion.module').then(m => m.ConfiguracionModule)
  },
  {
    path:"lista-de-juegos-terminados",
    loadChildren:()=>import('./juegos/juegos.module').then(m => m.JuegosModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
