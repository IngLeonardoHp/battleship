import { Component, OnInit } from '@angular/core';
import { Partidas } from '../models/partidas';
import { UserService } from '../services/auth/user.service';

@Component({
  selector: 'app-juegos',
  templateUrl: './juegos.component.html',
  styleUrls: ['./juegos.component.scss']
})
export class JuegosComponent implements OnInit {

  partidas:Partidas[]
  constructor(public userService:UserService) {
    this.partidas = [];
    this.partidas = this.userService.getPartidas();
    console.log(this.partidas);
  }

  ngOnInit(): void {
  }

}
