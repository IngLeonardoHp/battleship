import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JuegosRoutingModule } from './juegos-routing.module';
import { JuegosComponent } from './juegos.component';
import { ComponentesGlobalesModule } from '../componentes-globales/componentes-globales.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatListModule } from '@angular/material/list';


@NgModule({
  declarations: [
    JuegosComponent
  ],
  imports: [
    CommonModule,
    JuegosRoutingModule,
    FlexLayoutModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    MatSnackBarModule,
    ComponentesGlobalesModule,
    MatDialogModule,
    MatListModule
  ],
  exports:[ComponentesGlobalesModule]
})
export class JuegosModule { }
