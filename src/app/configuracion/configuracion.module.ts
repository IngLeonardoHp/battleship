import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfiguracionRoutingModule } from './configuracion-routing.module';
import { ConfiguracionComponent } from './configuracion.component';
import { ComponentesGlobalesModule } from '../componentes-globales/componentes-globales.module';


@NgModule({
  declarations: [
    ConfiguracionComponent
  ],
  imports: [
    CommonModule,
    ConfiguracionRoutingModule,
    ComponentesGlobalesModule
  ],
  exports:[ComponentesGlobalesModule]
})
export class ConfiguracionModule { }
