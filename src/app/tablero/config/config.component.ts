import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Level } from 'src/app/models/level';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.css']
})
export class ConfigComponent implements OnInit {

  levelarray:Level[];
  turnosPersonalizados:number|null;
  constructor(public dialogRef: MatDialogRef<ConfigComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.turnosPersonalizados = null;
    this.levelarray = [
      {
        level:1,
        name:"Nivel 1 - Fácil: Turnos indefinidos",
        turnos:null,
      },
      {
        level:2,
        name:"Nivel 2 - Medio: 100 turnos",
        turnos:100,
      },
      {
        level:3,
        name:"Nivel 3 - Difícil: 50 turnos",
        turnos:50,
      }
    ];
  }

  selectLevel(level:Level){
    this.dialogRef.close(level);
  }

  ngOnInit(): void {
  }

  continuar(){
    if(this.turnosPersonalizados && this.turnosPersonalizados>0){
      this.dialogRef.close({
        level:0,
        name:"Personalizado",
        turnos:this.turnosPersonalizados,
      });
    }else{
      alert("Debe ingresar al menos un turno");
    }
  }

}
