import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TableroRoutingModule } from './tablero-routing.module';
import { TableroComponent } from './tablero.component';
import { JugarComponent } from './jugar/jugar.component';
import { NickComponent } from './nick/nick.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms'; 
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ComponentesGlobalesModule } from '../componentes-globales/componentes-globales.module';
import { BloqueComponent } from './bloque/bloque.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ConfigComponent } from './config/config.component';
import { MatListModule } from '@angular/material/list';
import { LostComponent } from './lost/lost.component';
import { WinComponent } from './win/win.component';

@NgModule({
  declarations: [
    TableroComponent,
    JugarComponent,
    NickComponent,
    BloqueComponent,
    ConfigComponent,
    LostComponent,
    WinComponent
  ],
  imports: [
    CommonModule,
    TableroRoutingModule,
    FlexLayoutModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    MatSnackBarModule,
    ComponentesGlobalesModule,
    MatDialogModule,
    MatListModule
  ],
  exports:[ComponentesGlobalesModule]
})
export class TableroModule { }
