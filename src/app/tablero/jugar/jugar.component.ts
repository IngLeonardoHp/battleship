import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { Level } from 'src/app/models/level';
import { Partidas } from 'src/app/models/partidas';
import { UserService } from 'src/app/services/auth/user.service';
import { ConfigComponent } from '../config/config.component';
import { LostComponent } from '../lost/lost.component';
import { WinComponent } from '../win/win.component';

@Component({
  selector: 'app-jugar',
  templateUrl: './jugar.component.html',
  styleUrls: ['./jugar.component.scss']
})
export class JugarComponent implements OnInit {

  tableroTamanio:number
  boat:any[];
  matriz:any[][];
  playing:boolean=false;
  level:Level;
  turnosJugados:number;
  barcosDestruidos:number;
  constructor(public dialog: MatDialog, public userService:UserService) {
    this.tableroTamanio = 10;
    this.matriz = [];
    this.barcosDestruidos = 0;
    this.turnosJugados = 0;
    this.level =  new Level();
    this.boat = [];
    this.changeTablero(this.tableroTamanio);
    this.openConfig();
  }

  changeTablero(tamanio:number){  
    this.tableroTamanio = tamanio;
    this.barcosDestruidos = 0;
    this.matriz = [];
    for (let i = 0; i < this.tableroTamanio; i++) {
      this.matriz.push([]);
      for (let j = 0; j < this.tableroTamanio; j++) {
        this.matriz[i].push("");
      }
    }
  }

  openConfig(): void {
    const dialogRef = this.dialog.open(ConfigComponent, {
      width: '350px',
    });

    dialogRef.afterClosed().subscribe((result:Level) => {
      if(result){
        this.level = result;
        this.createBoat(4,"Titan");
        for (let i = 0; i < 2; i++) {
          this.createBoat(3,"Coloso"+i);
        }
        for (let i = 0; i < 3; i++) {
          this.createBoat(2,"Acaorasado"+i);
        }
        for (let i = 0; i < 4; i++) {
          this.createBoat(1,"Pesqueros"+i);
        }
        console.log(this.matriz,this.boat);
        this.playing = true;
      }
    });
  }

  createBoat(espacios:number,nombre:string){
    console.log("entro aqui")
    let posX = this.getRandomInt(0,10);
    let posY = this.getRandomInt(0,10);
    if(this.matriz[posY][posX]==""){
      let direccion = this.getRandomInt(0,5);
      let valida = this.validate(direccion,espacios,posX,posY);
      if(valida){
        let pos = valida.pos;
        pos = [posY+","+posX].concat(pos);
        this.matriz = valida.matrix;
        this.matriz[posY][posX]="B";
        this.boat.push({barco:{
          nombre:nombre,
          pos:pos,
          posR:pos,
          destruido:false,
        }});
      }else{
        this.createBoat(espacios,nombre);
      }
    }else{
      this.createBoat(espacios,nombre);
    }
  }

  getRandomInt(min:number, max:number) {
    return Math.floor(Math.random() * (max - min)) + min;
  }

  posicionValida(i:number,j:number) {
    return i>=0 && j>=0 && i<this.matriz.length && j<this.matriz[i].length
  } 

  validate(direccion:number,espacios:number,x:number,y:number){
    let matrizTemporal = JSON.parse(JSON.stringify(this.matriz));
    let retornar = true;
    let pos = [];
    switch (direccion) {
      case 0://abajo
        for (let i = 0; i < (espacios-1); i++) {
          if(this.posicionValida(y+(i+1),x)){
            matrizTemporal[y+(i+1)][x] = "A";
            pos.push((y+(i+1))+","+x);
          }else{
            pos = [];
            retornar = false;
            break;
          }
        }
        break;
      case 1://derecha
        for (let i = 0; i < (espacios-1); i++) {
          if(this.posicionValida(y,x+(i+1))){
            matrizTemporal[y][x+(i+1)] = "D";
            pos.push(y+","+(x+(i+1)));
          }else{
            pos = [];
            retornar = false;
            break;
          }
        }
        break;
      case 2://izquierda
        for (let i = 0; i < (espacios-1); i++) {
          if(this.posicionValida(y,x-(i+1))){
            matrizTemporal[y][x-(i+1)] = "I";
            pos.push(y+","+(x-(i+1)));
          }else{
            pos = [];
            retornar = false;
            break;
          }
        }
        break;
      case 3://arriba
        for (let i = 0; i < (espacios-1); i++) {
          if(this.posicionValida(y-(i+1),x)){
            matrizTemporal[y-(i+1)][x] = "AB";
            pos.push(y-(i+1)+","+x);
          }else{
            pos = [];
            retornar = false;
            break;
          }
        }
        break;
        default:
          console.log("arriba");
          for (let i = 0; i < (espacios-1); i++) {
            if(this.posicionValida(y-(i+1),x)){
              matrizTemporal[y-(i+1)][x] = "AB";
              pos.push(y-(i+1)+","+x);
            }else{
              pos = [];
              retornar = false;
              break;
            }
          }
        break;
    }
    if(retornar){
      return {matrix:matrizTemporal,pos:pos};
    }else{
      return false;
    }
  }

  reset(){
    this.turnosJugados=0;
    this.changeTablero(10);
    this.openConfig();
    this.boat = [];
  }

  ngOnInit(): void {
  }

  hit(i:number,j:number){
    if(this.matriz[j][i]!='x' && this.matriz[j][i]!='f'){
      if(this.matriz[j][i]==""){
        this.matriz[j][i]="f";
      }else{
        this.matriz[j][i]="x";
        for (let k = 0; k < this.boat.length; k++) {
          let pos = [];
          for (let l = 0; l < this.boat[k].barco.pos.length; l++) {
            if(this.boat[k].barco.pos[l]!=j+","+i){
              pos.push(this.boat[k].barco.pos[l]);
            }
          }
          this.boat[k].barco.pos = pos;
          if(this.boat[k].barco.pos.length==0){
            this.boat[k].barco.destruido = true;
            this.pintarBarco(this.boat[k]);
          }
        }
        console.log(this.boat);
      }
      this.turnosJugados++;
    }
    if(this.level.turnos && this.turnosJugados>=this.level.turnos){
      this.saveHistory(false);
      const dialogRef = this.dialog.open(LostComponent, {
        width: '350px',
      });
  
      dialogRef.afterClosed().subscribe((result:any) => {
        if(result){
          this.reset();
        }
      });
    }
    this.barcosDestruidosFuncion();
  }


  barcosDestruidosFuncion(){
    this.barcosDestruidos = 0;
    for (let k = 0; k < this.boat.length; k++) {
      if(this.boat[k].barco.pos.length==0){
        this.barcosDestruidos++;
      }
    }
    if(this.barcosDestruidos>=this.boat.length){
      this.saveHistory(true);
      const dialogRef = this.dialog.open(WinComponent, {
        width: '350px',
      });
  
      dialogRef.afterClosed().subscribe((result:any) => {
        if(result){
          this.reset();
        }
      });
    }
  }

  pintarBarco(array:any){
    console.log(array);
    for (let i = 0; i < array.barco.posR.length; i++) {
      let postemp = array.barco.posR[i].split(",");
      let posY = postemp[0];
      let posX = postemp[1];
      console.log(posY,posX);
      this.matriz[posY][posX]="XX";
    }
  }

  saveHistory(win:boolean=false){
    let partida = new Partidas();
    partida.tablero = this.matriz;
    partida.level = this.level;
    partida.win = win;
    partida.turnosJugados = this.turnosJugados;
    this.userService.setPartidas(partida);
  }

  letra(i:number){
    switch ((i+1)) {
      case 1: return "A";
      case 2: return "B";
      case 3: return "C";
      case 4: return "D";
      case 5: return "E";
      case 6: return "F";
      case 7: return "G";
      case 8: return "H";
      case 9: return "I";
      case 10: return "J";
      case 11: return "K";
      case 12: return "L";
      case 13: return "M";
      case 14: return "N";
      case 15: return "Ñ";
      case 16: return "O";
      case 17: return "P";
      case 18: return "Q";
      case 19: return "R";
      case 20: return "S";
      case 21: return "T";
      case 22: return "U";
      case 23: return "V";
      case 24: return "W";
      case 25: return "X";
      case 26: return "Y";
      case 27: return "Z";
      default: return "#"
    }
    return "";
  }
}
