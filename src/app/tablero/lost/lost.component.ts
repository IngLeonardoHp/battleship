import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-lost',
  templateUrl: './lost.component.html',
  styleUrls: ['./lost.component.css']
})
export class LostComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<LostComponent>) { }

  ngOnInit(): void {
  }

  continuar(){
    this.dialogRef.close(true);
  }

}
