import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/auth/user.service';

@Component({
  selector: 'app-nick',
  templateUrl: './nick.component.html',
  styleUrls: ['./nick.component.scss']
})
export class NickComponent implements OnInit {

  user:User;
  constructor(public userService:UserService, public router:Router, public _snackBar: MatSnackBar) {
    this.user = new User();
    this.user = this.userService.getUser();
  }

  ngOnInit(): void {
  }

  continuar(){
    if(this.user.nick.length>0){
      this.userService.setUser(this.user);
      this.router.navigate([""]);
    }else{
      this._snackBar.open("Debe ingresar un nick de al menos una letra ;)","Ok",{duration:3000})
    }
  }

  validarEnter(event:any){
    if(event.key == 'Enter'){
      this.continuar();
    }
  }

}
