import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JugarComponent } from './jugar/jugar.component';
import { NickComponent } from './nick/nick.component';
import { TableroComponent } from './tablero.component';

const routes: Routes = [
  {
    path:"",
    component:TableroComponent,
    children:[
      {
        path:"",
        component:JugarComponent
      },
      {
        path:"nick",
        component:NickComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TableroRoutingModule { }
