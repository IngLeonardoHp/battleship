import { Injectable } from '@angular/core';
import { Partidas } from 'src/app/models/partidas';
import { User } from 'src/app/models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  user:User;
  partidas:Partidas[];
  constructor() {
    this.user = new User();
    this.partidas = [];
  }

  setUser(data:User){
    this.user.nick = data.nick;
    localStorage.setItem("user",JSON.stringify(this.user));
  }

  getUser(){
    let userString:any = localStorage.getItem("user") ? localStorage.getItem("user"):'';
    if(userString==''){
      return false;
    }
    return JSON.parse(userString);
  }

  setPartidas(partidas:Partidas){
    let partidasT:any = localStorage.getItem("partidas") ? localStorage.getItem("partidas"):'';
    if(partidasT!=''){
      this.partidas = JSON.parse(partidasT);
    }
    this.partidas.push(partidas);
    localStorage.setItem("partidas",JSON.stringify(this.partidas))
  }

  getPartidas(){
    let partidasT:any = localStorage.getItem("partidas") ? localStorage.getItem("partidas"):'';
    if(partidasT!=''){
      this.partidas = JSON.parse(partidasT);
    }else{
      this.partidas = [];
    }
    return this.partidas;
  }
}
